-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Czas generowania: 07 Lut 2017, 15:32
-- Wersja serwera: 10.1.19-MariaDB
-- Wersja PHP: 5.6.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `quizmaster`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `category`
--

CREATE TABLE `category` (
  `categoryID` int(11) NOT NULL,
  `name` text CHARACTER SET utf8 COLLATE utf8_polish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Zrzut danych tabeli `category`
--

INSERT INTO `category` (`categoryID`, `name`) VALUES
(1, 'Sport'),
(2, 'Geografia'),
(3, 'Polityka'),
(4, 'Historia'),
(5, 'Religia'),
(6, 'Nauka i wynalazki'),
(7, 'Telewizja'),
(8, 'Język polski'),
(9, 'Literatura'),
(10, 'Muzyka'),
(11, 'Gry komputerowe'),
(12, 'Komputery i internet'),
(13, 'Filmy'),
(14, 'Znani ludzie'),
(15, 'Przyroda'),
(16, 'Powiedzenia i przysłowia'),
(17, 'Matematyka'),
(18, 'Motoryzacja');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `question`
--

CREATE TABLE `question` (
  `questionID` int(11) NOT NULL,
  `body` text CHARACTER SET utf8 COLLATE utf8_polish_ci NOT NULL,
  `A` text CHARACTER SET utf8 COLLATE utf8_polish_ci NOT NULL,
  `B` text CHARACTER SET utf8 COLLATE utf8_polish_ci NOT NULL,
  `C` text CHARACTER SET utf8 COLLATE utf8_polish_ci NOT NULL,
  `D` text CHARACTER SET utf8 COLLATE utf8_polish_ci NOT NULL,
  `correct_answer` text CHARACTER SET utf8 COLLATE utf8_polish_ci NOT NULL,
  `categoryID` int(11) NOT NULL,
  `quizID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Zrzut danych tabeli `question`
--

INSERT INTO `question` (`questionID`, `body`, `A`, `B`, `C`, `D`, `correct_answer`, `categoryID`, `quizID`) VALUES
(586, 'Jaka jest stolica Słowacji?', 'Radom', 'Budapeszt', 'Wenecja', 'Bratysława', 'D', 2, 152),
(587, 'Jaka jest stolica Hiszpanii?', 'Madryt', 'Mińsk', 'Barcelona', 'Moskwa', 'A', 2, 152),
(588, 'Jaka jest stolica Serbii?', 'Monachium', 'Belgrad', 'Prisztina', 'Praga', 'B', 2, 152),
(589, 'Jaka jest stolica Włoch?', 'Rzym', 'San Marino', 'Wenecja', 'Ateny', 'A', 2, 152),
(590, 'Jaka jest Stolica Meksyku?', 'Waszyngton', 'Meksyk', 'Montevideo', 'Lima', 'B', 2, 153),
(591, 'Jaka jest stolica Hondurasu?', 'Gwatemala', 'Panama', 'Wałbrzych', 'Honduras', 'D', 2, 153),
(592, 'Jaka jest stolica Peru?', 'Quito', 'Lima', 'Asuncion', 'Nowy Jork', 'B', 2, 153),
(593, 'Jaka jest stolica Kanady?', 'Wisconsin', 'Miami', 'Ottawa', 'Salwador', 'C', 2, 153),
(594, 'Jaka jest stolica Kuby?', 'Hawana', 'Santiago de Cuba', 'Managua', 'San Domingo', 'A', 2, 153),
(595, 'Jaka jest stolica Panamy?', 'San Pedro Sula', 'Tegucigalpa', 'Portoryko', 'Panama', 'D', 2, 153),
(596, 'Kto jest reżyserem filmu "Zielona Mila"?', 'Frank Darabont', 'Jonathan Demme', 'David Fincher', 'Roman Polański', 'A', 13, 154),
(597, 'Kto jest reżyserem filmu "Ojciec Chrzestny"?', 'Eli Roth', 'Brian De Palma', 'Francis Ford Coppola', 'Luc Besson', 'C', 13, 154),
(598, 'Kto jest reżyserem filmu "Skazani na Shawshank"?', 'Steven Spielberg', 'Frank Darabont', 'Barry Levinson', 'Jonathan Demme', 'B', 13, 154),
(599, 'Kto jest reżyserem filmu "Władca Pierścieni: Powrót Króla"?', 'Liam Neeson', 'Steven Spielberg', 'Alfred Hitchcock', 'Peter Jackson', 'D', 13, 154),
(600, 'Kto jest reżyserem filmu "Pulp Fiction"?', 'Barry Levinson', 'Quentin Tarantino', 'Woody Allen', 'Luc Besson', 'B', 13, 154),
(601, 'Kto jest reżyserem filmu "Podziemny krąg"?', 'David Fincher', 'Alfred Hitchcock', 'Eli Roth', 'Brian De Palma', 'A', 13, 154),
(602, 'Jak przetłumaczono tytuł filmu "Fight Club"?', '"Klub Wojowników"', '"Podziemny Krąg"', '"Krąg Wojowników"', '"Waleczna Drużyna"', 'B', 13, 155),
(603, 'Jak przetłumaczono tytuł filmu "Die Hard"?', '"Żyć Do Końca"', '"Szklana Pułapka"', '"Pułapka Śmierci"', '"Umierając"', 'B', 13, 155),
(604, 'Jak przetłumaczono tytuł filmu "The Lion King"?', '"Król Lew"', '"Król Dżungli"', '"Lwia Opowieść"', '"Lwia Przygoda"', 'A', 13, 155),
(605, 'Jak przetłumaczono tytuł filmu "Saving Private Ryan"?', '"Żołnierska Opowieść"', '"Historia Ryana"', '"Ocalić Ryana"', '"Szeregowiec Ryan"', 'D', 13, 155),
(606, 'Ile tytułów wielkoszlemowych ma W swym dorobku Rafael Nadal?', '12', '16', '9', '14', 'D', 1, 156),
(607, 'Który z polskich skoczków narciarskich nazywany jest przez kolegów Wiewiór?', 'Piotr Żyła', 'Kamil Stoch', 'Maciej Kot', 'Dawid Kubacki', 'A', 1, 156),
(608, 'Jakim pseudonimem posługuje się angielski snookerzysta Ronnie O’Sullivan?', 'The Wizard', 'The Rocket', 'The Warrior', 'The Ace', 'B', 1, 156),
(609, 'Który z poniższych NIE jest słynnym snookerzystą?', 'Marco Fu', 'Neil Robertson', 'Jeff Bloom', 'Ding Junhui', 'C', 1, 156),
(610, 'W jakim klubie gra obecnie Robert Lewandowski?', 'Chelsea Londyn', 'Real Madryt', 'FC Barcelona', 'Bayern Monachium', 'D', 1, 156),
(611, 'W jakim klubie piłkarskim nie grał francuski piłkarz Zinedine Zidane?', 'Inter Mediolan', 'Real Madryt', 'Juventus Turyn', 'Girondins Bordeaux', 'A', 1, 156),
(612, 'Która drużyna gra na stadionie przy ulicy Kałuży?', 'Legia Warszawa', 'Cracovia Kraków', 'Wisła Kraków', 'Śląsk Wrocław', 'B', 1, 157),
(613, 'W jakim klubie grał Roger Guerreiro?', 'Lech Poznań', 'Arka Gdynia', 'Legia Warszawa', 'Górnik Łęczna', 'C', 1, 157),
(614, 'Ile drużyn nacodzień występuje w Ekstraklasie?', '12', '14', '16', '18', 'C', 1, 157),
(615, 'Jak nazywa się główny bohater gry RPG akcji “Mass Effect”?', 'Admirał Fields', 'Sierżant Anderson', 'Porucznik Collins', 'Komandor Shepard', 'D', 11, 158),
(616, 'Jak nazywa się brat Mario z gry Super Mario?', 'Anton', 'Luigi', 'Stefan', 'Lucas', 'B', 11, 158),
(617, 'Który z bohaterów gry Resident Evil należał do oddziału S.T.A.R.S.?', 'Albert Wesker', 'Wiliam Birkin', 'Sherry Birkin', 'Leon Kennedy', 'D', 11, 158),
(618, 'Bohaterem jakiej gry jest Carl Johnson?', 'GTA San Andreas', 'GTA Vice City', 'GTA 3', 'GTA 4', 'A', 11, 158),
(619, '"Czy to ptak? Czy to samolot?', '"Batman: Początek"', '"Superman"', '"Spider-Man"', '"X-Men"', 'B', 13, 159),
(620, '"Zjadłem jego wątrobę z bobem popijając smacznym chianti"', '"Nietykalni"', '"Zielona Mila"', '"Milczenie Owiec"', '"Dexter"', 'C', 13, 159),
(621, '"Albo umrzesz jako bohater, albo będziesz żył wystarczająco długo, by spostrzec iż stałeś się czarnym charakterem." ', '"Mroczny Rycerz"', '"Batman: Początek"', '"Iron Man"', '"Avengers"', 'A', 13, 159),
(622, '"Wszystkie te chwile znikną z czasem... jak łzy w deszczu" ', '"Łowca androidów" ', '"Powrót do przyszłości" ', '"Bezsenność w Seattle" ', '"Biały oleander" ', 'A', 13, 159),
(623, '"Od lat coś w objęcia chłodu mnie pcha." ', '"Przygoda na Antarktydzie"', '"Śnieg Wojny"', '"Everest"', '"Kraina Lodu"', 'D', 13, 159),
(624, '"Przywitaj się z moim małym przyjacielem." ', '"Ojciec chrzestny"', '"Człowiek z blizną" ', '"Chłopcy z ferajny"', ' "Wściekłe psy"', 'B', 13, 159),
(625, '"To piękna zabiła bestię." ', '"King Kong"', '"Sin City - Miasto Grzechu"', '"Rocky"', '"Tarzan"', 'A', 13, 159),
(626, 'Życie jest jak pudełko czekoladek. Nigdy nie wiadomo co się trafi', '"Leon Zawodowiec"', '"Pulp Fiction"', '"Mission Impossible"', '"Forest Gump"', 'D', 13, 159),
(627, 'Jaka jest stolica RPA?', 'Niger', 'Pretoria', 'Konakry', 'Sabha', 'B', 2, 160),
(628, 'Jaka jest stolica Botswany?', 'Gaborone', 'Rabat', 'Tunis', 'Trypolis', 'A', 2, 160),
(629, 'Jaka jest stolica Mozambiku?', 'Chartum', 'Keren', 'Kampala', 'Maputo', 'D', 2, 160),
(630, 'Jaka jest stolica Angoli?', 'Windhuk', 'Luanda', 'Mazabuka', 'Londyn', 'B', 2, 160),
(631, 'Jaka jest stolica Gabonu?', 'Freeville', 'Antananarywa', 'Kabwa', 'Libreville', 'D', 2, 160),
(632, 'Jaka jest stolica Kamerunu?', 'Lusaka', 'Kananka', 'Jaunde', 'Gabanore', 'C', 2, 160),
(633, '"Kto dołki kopie, ten..."', 'Sam w nie wpada', 'ma łopatę', 'trafia do celu', 'przechytrzy innych', 'A', 16, 161),
(634, '"Bogatemu diabeł..."', 'w polu pomaga', 'kuje konie', 'dzieci kołysze', 'drogę wskazuje', 'C', 16, 161),
(635, 'Gdzie kucharek sześć, tam..."', 'duża kuchnia', 'opinii dwanaście', 'jest problem', 'nie ma co jeść', 'D', 16, 161),
(636, 'Pawiem narodów byłaś i papugą” – te słowa z utworu Juliusza Słowackiego odnoszą się do:', 'Francji', 'Rosji', 'Grecji', 'Polski', 'D', 9, 162),
(637, 'Jak nazywa się przedostatnia książka serii “Opowieści z Narnii”?', 'Siostrzeniec Czarodzieja', 'Koń i Jego Chłopiec', 'Ostatnia Bitwa', 'Książę Caspian', 'A', 9, 162),
(638, 'Kto był miłością Severusa Snape’a z serii “Harry Potter”?', 'Hermiona Granger', 'Albus Dumbledore', 'Lily Evans', 'Nie potrafił kochać', 'C', 9, 162),
(639, 'Jak nazywa się wokalista zespołu Pearl Jam?', 'Eddie Vedder', 'Piotr Kupicha', 'Chris Cornell', 'Kurt Cobain', 'A', 10, 163),
(640, 'Jak nazywa się wokalista zespołu H.I.M?', 'Axl Rose', 'Ville Valo', 'Joey Temprst', 'Marco Hietala', 'B', 10, 163),
(641, 'Kto był miłością Severusa Snape’a z serii “Harry Potter”?', 'Harry Potter', 'Hermiona Granger', 'Lily Evans', 'Albus Dumbledore', 'C', 9, 164),
(642, 'Pawiem narodów byłaś i papugą” – te słowa z utworu Juliusza Słowackiego odnoszą się do:', 'Rosji', 'Francji', 'Polski', 'Grecji', 'C', 9, 164),
(643, '“Miałeś, chamie, złoty róg, ostał ci się jeno sznur”. Słowa te padają w:', 'Wesele', 'Dziady', 'Pan Tadeusz', 'Dżuma', 'A', 9, 164),
(644, 'Jak nazywa się wokalista zespołu H.I.M?', 'Ville Valo', 'Axl Rose', 'Marco Hietala', 'Piotr Kupicha', 'A', 10, 165),
(645, 'Jak nazywa się wokalista zespołu Pearl Jam?', 'Joey Temprst', 'Eddie Vedder', 'Kurt Cobain', 'Chris Cornell', 'B', 10, 165),
(646, 'Jak nazywał się wokalista zespołu Joy Division?', 'Harvey Penn', 'Larry Brown', 'Jack Porter', 'lan Curtis', 'D', 10, 165),
(649, 'Jak ma na imię główny bohater Świata według Kiepskich?', 'Marian', 'Waldemar', 'Ferdynand', 'Stefan', 'C', 13, 167),
(650, 'Jak ma na nazwisko Marian, właściciel firmy budowlanej z Na Wspólnej?', 'Ferdynand', 'Marian', 'Stefan', 'Roman', 'D', 13, 167),
(651, 'Jak nazywał się pies z Czterej pancerni i pies?', 'Burek', 'Azor', 'Szarik', 'Bielik', 'C', 13, 167),
(652, 'Jak ma na imię żona Karola Krawczyka z Miodowych lat?', 'Alina', 'Danuta', 'Małgorzata', 'Paulina', 'A', 13, 167);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `quiz`
--

CREATE TABLE `quiz` (
  `quizID` int(11) NOT NULL,
  `name` text COLLATE utf8_polish_ci NOT NULL,
  `description` text COLLATE utf8_polish_ci NOT NULL,
  `categoryID` int(11) NOT NULL,
  `userID` int(11) NOT NULL,
  `isAccepted` tinyint(1) NOT NULL,
  `dateCreated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `quiz`
--

INSERT INTO `quiz` (`quizID`, `name`, `description`, `categoryID`, `userID`, `isAccepted`, `dateCreated`) VALUES
(152, 'Stolice Europy', 'Sprawdź swoją wiedzę na temat stolic państw Europy!', 2, 10, 1, '2017-02-01 00:20:11'),
(153, 'Stolice Ameryk', 'Sprawdź swoją wiedzę na temat stolic państw obu Ameryk!', 2, 10, 1, '2017-02-01 00:20:11'),
(154, 'Czy znasz reżysera?', 'Sprawdź czy znasz reżyserów sławnych filmów!', 13, 10, 1, '2017-02-01 00:48:12'),
(155, 'Jak przetłumaczono tytuły filmów?', 'Czy zgadniesz jak przetłumaczono tytuły podanych filmów na język polski?', 13, 10, 1, '2017-02-01 00:48:12'),
(156, 'Znani sportowcy', 'Sprawdź co wiesz o znanych sportowcach.', 1, 1, 1, '2017-02-01 00:48:12'),
(157, 'Szybki quiz ekstraklasowy', '3 szybkie pytania na temat piłkarskiej Ekstraklasy', 1, 1, 1, '2017-02-01 00:48:12'),
(158, 'Bohaterowie gier komputerowych', 'Czy znasz głównych protagonistów z popularnych gier?', 11, 1, 1, '2017-02-01 00:59:26'),
(159, 'Cytaty filmowe', 'Sprawdź czy wiesz z jakiego filmu pochodzą cytaty!', 13, 10, 1, '2017-02-01 01:17:11'),
(160, 'Stolice Afryki', 'Czy znasz stolice Państw Afryki?', 2, 10, 1, '2017-02-01 01:17:11'),
(161, 'Dokończ Przysłowie', 'Sprawdź czy znasz polskie przysłowia!', 16, 10, 1, '2017-02-01 01:17:11'),
(164, 'Quiz książkowy', 'Sprawdź swoją wiedzę o książkach', 9, 10, 1, '2017-02-01 01:43:35'),
(165, 'Quiz muzyczny', 'Sprawdź swoją wiedzę na temat gwiazd estrady!', 10, 10, 1, '2017-02-01 01:53:47'),
(167, 'Polskie seriale', 'Czy znasz bohaterów popularnych polskich seriali?', 13, 11, 0, '2017-02-01 23:05:07');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `quiz_answer`
--

CREATE TABLE `quiz_answer` (
  `quiz_answerID` int(11) NOT NULL,
  `quiz_factID` int(11) NOT NULL,
  `questionID` int(11) NOT NULL,
  `user_answer` text CHARACTER SET utf8 COLLATE utf8_polish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Zrzut danych tabeli `quiz_answer`
--

INSERT INTO `quiz_answer` (`quiz_answerID`, `quiz_factID`, `questionID`, `user_answer`) VALUES
(16, 26, 644, 'A'),
(17, 26, 645, 'B'),
(18, 26, 646, 'C'),
(19, 27, 619, 'A'),
(20, 27, 620, 'B'),
(21, 27, 621, 'B'),
(22, 27, 622, 'D'),
(23, 27, 623, 'A'),
(24, 27, 624, 'B'),
(25, 27, 625, 'C'),
(26, 27, 626, 'D'),
(27, 28, 644, 'A'),
(28, 28, 645, 'B'),
(29, 28, 646, 'C'),
(30, 29, 641, 'A'),
(31, 29, 642, 'C'),
(32, 29, 643, 'A'),
(33, 30, 619, 'D'),
(34, 30, 620, 'A'),
(35, 30, 621, 'C'),
(36, 30, 622, 'C'),
(37, 30, 623, 'B'),
(38, 30, 624, 'B'),
(39, 30, 625, 'C'),
(40, 30, 626, 'D'),
(41, 31, 615, 'D'),
(42, 31, 616, 'B'),
(43, 31, 617, 'A'),
(44, 31, 618, 'C'),
(45, 32, 606, 'B'),
(46, 32, 607, 'D'),
(47, 32, 608, 'A'),
(48, 32, 609, 'B'),
(49, 32, 610, 'D'),
(50, 32, 611, 'A'),
(51, 33, 602, 'B'),
(52, 33, 603, 'B'),
(53, 33, 604, 'A'),
(54, 33, 605, 'D'),
(55, 34, 612, 'B'),
(56, 34, 613, 'C'),
(57, 34, 614, 'A'),
(58, 35, 641, 'A'),
(59, 35, 642, 'C'),
(60, 35, 643, 'C'),
(61, 36, 619, 'B'),
(62, 36, 620, 'C'),
(63, 36, 621, 'A'),
(64, 36, 622, 'A'),
(65, 36, 623, 'D'),
(66, 36, 624, 'B'),
(67, 36, 625, 'A'),
(68, 36, 626, 'D'),
(69, 37, 619, 'B'),
(70, 37, 620, 'C'),
(71, 37, 621, 'A'),
(72, 37, 622, 'A'),
(73, 37, 623, 'D'),
(74, 37, 624, 'B'),
(75, 37, 625, 'A'),
(76, 37, 626, 'D'),
(77, 38, 644, 'C'),
(78, 38, 645, 'B'),
(79, 38, 646, 'C'),
(80, 39, 627, 'B'),
(81, 39, 628, 'C'),
(82, 39, 629, 'C'),
(83, 39, 630, 'B'),
(84, 39, 631, 'B'),
(85, 39, 632, 'A'),
(86, 40, 586, 'A'),
(87, 40, 587, 'B'),
(88, 40, 588, 'C'),
(89, 40, 589, 'B'),
(90, 41, 596, 'A'),
(91, 41, 597, 'B'),
(92, 41, 598, 'A'),
(93, 41, 599, 'B'),
(94, 41, 600, 'C'),
(95, 41, 601, 'B'),
(96, 42, 644, 'B'),
(97, 42, 645, 'B'),
(98, 42, 646, 'A'),
(99, 43, 633, 'B'),
(100, 43, 634, 'A'),
(101, 43, 635, 'D'),
(102, 44, 590, 'B'),
(103, 44, 591, 'D'),
(104, 44, 592, 'B'),
(105, 44, 593, 'C'),
(106, 44, 594, 'A'),
(107, 44, 595, 'D'),
(108, 45, 627, 'B'),
(109, 45, 628, 'A'),
(110, 45, 629, 'C'),
(111, 45, 630, 'B'),
(112, 45, 631, 'D'),
(113, 45, 632, 'D'),
(114, 46, 633, 'A'),
(115, 46, 634, 'C'),
(116, 46, 635, 'D'),
(117, 47, 615, 'D'),
(118, 47, 616, 'B'),
(119, 47, 617, 'C'),
(120, 47, 618, 'A'),
(121, 48, 602, 'B'),
(122, 48, 603, 'B'),
(123, 48, 604, 'A'),
(124, 48, 605, 'D'),
(125, 49, 606, 'A'),
(126, 49, 607, 'B'),
(127, 49, 608, 'D'),
(128, 49, 609, 'D'),
(129, 49, 610, 'D'),
(130, 49, 611, 'A'),
(131, 50, 641, 'C'),
(132, 50, 642, 'C'),
(133, 50, 643, 'A'),
(134, 51, 619, 'B'),
(135, 51, 620, 'C'),
(136, 51, 621, 'D'),
(137, 51, 622, 'D'),
(138, 51, 623, 'D'),
(139, 51, 624, 'B'),
(140, 51, 625, 'B'),
(141, 51, 626, 'D'),
(142, 52, 606, 'D'),
(143, 52, 607, 'B'),
(144, 52, 608, 'B'),
(145, 52, 609, 'D'),
(146, 52, 610, 'C'),
(147, 52, 611, 'A'),
(148, 53, 633, 'A'),
(149, 53, 634, 'C'),
(150, 53, 635, 'C'),
(151, 54, 586, 'D'),
(152, 54, 587, 'A'),
(153, 54, 588, 'B'),
(154, 54, 589, 'A'),
(155, 55, 596, 'A'),
(156, 55, 597, 'D'),
(157, 55, 598, 'A'),
(158, 55, 599, 'A'),
(159, 55, 600, 'B'),
(160, 55, 601, 'A'),
(161, 56, 619, 'B'),
(162, 56, 620, 'C'),
(163, 56, 621, 'D'),
(164, 56, 622, 'A'),
(165, 56, 623, 'C'),
(166, 56, 624, 'D'),
(167, 56, 625, 'A'),
(168, 56, 626, 'B'),
(169, 57, 641, 'B'),
(170, 57, 642, 'A'),
(171, 57, 643, 'A'),
(172, 58, 615, 'B'),
(173, 58, 616, 'C'),
(174, 58, 617, 'B'),
(175, 58, 618, 'A'),
(176, 59, 644, 'C'),
(177, 59, 645, 'A'),
(178, 59, 646, 'A'),
(179, 60, 590, 'C'),
(180, 60, 591, 'C'),
(181, 60, 592, 'C'),
(182, 60, 593, 'D'),
(183, 60, 594, 'D'),
(184, 60, 595, 'D'),
(185, 61, 633, 'D'),
(186, 61, 634, 'B'),
(187, 61, 635, 'A'),
(188, 62, 644, 'C'),
(189, 62, 645, 'B'),
(190, 62, 646, 'B'),
(191, 63, 644, 'A'),
(192, 63, 645, 'D'),
(193, 63, 646, 'D'),
(194, 64, 619, 'B'),
(195, 64, 620, 'C'),
(196, 64, 621, 'A'),
(197, 64, 622, 'A'),
(198, 64, 623, 'D'),
(199, 64, 624, 'B'),
(200, 64, 625, 'A'),
(201, 64, 626, 'D'),
(202, 65, 596, 'B'),
(203, 65, 597, 'A'),
(204, 65, 598, 'A'),
(205, 65, 599, 'A'),
(206, 65, 600, 'C'),
(207, 65, 601, 'C'),
(208, 66, 641, 'A'),
(209, 66, 642, 'A'),
(210, 66, 643, 'C'),
(211, 67, 641, 'B'),
(212, 67, 642, 'A'),
(213, 67, 643, 'B'),
(214, 68, 602, 'A'),
(215, 68, 603, 'B'),
(216, 68, 604, 'A'),
(217, 68, 605, 'D');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `quiz_fact`
--

CREATE TABLE `quiz_fact` (
  `quiz_factID` int(11) NOT NULL,
  `quizID` int(11) NOT NULL,
  `userID` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Zrzut danych tabeli `quiz_fact`
--

INSERT INTO `quiz_fact` (`quiz_factID`, `quizID`, `userID`, `date`) VALUES
(26, 165, 1, '2017-02-01 21:09:19'),
(27, 159, 1, '2017-02-01 21:09:47'),
(28, 165, 10, '2017-02-01 22:22:29'),
(29, 164, 10, '2017-02-01 22:22:44'),
(30, 159, 10, '2017-02-01 22:22:59'),
(31, 158, 10, '2017-02-01 22:24:07'),
(32, 156, 13, '2017-02-01 23:43:27'),
(33, 155, 13, '2017-02-01 23:44:00'),
(34, 157, 13, '2017-02-01 23:44:29'),
(35, 164, 14, '2017-02-01 23:45:17'),
(36, 159, 14, '2017-02-01 23:45:41'),
(37, 159, 14, '2017-02-01 23:46:09'),
(38, 165, 14, '2017-02-01 23:46:23'),
(39, 160, 16, '2017-02-01 23:47:17'),
(40, 152, 16, '2017-02-01 23:47:34'),
(41, 154, 16, '2017-02-01 23:47:59'),
(42, 165, 16, '2017-02-01 23:48:10'),
(43, 161, 16, '2017-02-01 23:48:44'),
(44, 153, 15, '2017-02-01 23:49:48'),
(45, 160, 15, '2017-02-01 23:50:25'),
(46, 161, 15, '2017-02-01 23:50:55'),
(47, 158, 15, '2017-02-01 23:51:14'),
(48, 155, 15, '2017-02-01 23:51:30'),
(49, 156, 17, '2017-02-01 23:55:23'),
(50, 164, 17, '2017-02-01 23:55:33'),
(51, 159, 17, '2017-02-01 23:55:55'),
(52, 156, 17, '2017-02-01 23:56:25'),
(53, 161, 18, '2017-02-01 23:58:44'),
(54, 152, 18, '2017-02-01 23:59:39'),
(55, 154, 18, '2017-02-02 00:00:10'),
(56, 159, 19, '2017-02-02 00:00:45'),
(57, 164, 19, '2017-02-02 00:00:54'),
(58, 158, 19, '2017-02-02 00:01:06'),
(59, 165, 20, '2017-02-02 00:02:36'),
(60, 153, 20, '2017-02-02 00:02:51'),
(61, 161, 20, '2017-02-02 00:02:59'),
(62, 165, 20, '2017-02-02 00:03:06'),
(63, 165, 20, '2017-02-02 00:03:12'),
(64, 159, 10, '2017-02-02 00:41:14'),
(65, 154, 1, '2017-02-02 00:59:09'),
(66, 164, 1, '2017-02-02 01:55:03'),
(67, 164, 1, '2017-02-06 13:18:37'),
(68, 155, 1, '2017-02-07 08:05:42');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `user`
--

CREATE TABLE `user` (
  `userID` int(11) NOT NULL,
  `login` text COLLATE utf8_polish_ci NOT NULL,
  `password` text COLLATE utf8_polish_ci NOT NULL,
  `email` text COLLATE utf8_polish_ci NOT NULL,
  `regDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `rank` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `user`
--

INSERT INTO `user` (`userID`, `login`, `password`, `email`, `regDate`, `rank`) VALUES
(1, 'HunT', 'test', 'test@test.pl', '2017-01-31 14:46:29', 1),
(10, 'kula', 'kula', 'piotrkolebski@gmail.com', '2017-01-30 18:58:28', 0),
(11, 'marian', 'marian123', 'marian@gmail.com', '2017-02-01 23:00:05', 0),
(12, 'fan_mariana', 'marian123', 'fan_mariana@gmail.ocm', '2017-02-01 23:05:38', 0),
(13, 'uzytkownik123', '123', 'uzytkownik123@email.com', '2017-02-01 23:33:05', 0),
(14, 'mariolka', '123', 'mariolka@wrobelek.pl', '2017-02-01 23:33:34', 0),
(15, 'mistrz_quizow', '123', 'mistrz_quizow@quiz.pl', '2017-02-01 23:33:48', 0),
(16, 'student93', '123', 'student@student.pwr.pl', '2017-02-01 23:34:20', 0),
(17, 'quizowy_wariat', '123', 'quizik@wp.pl', '2017-02-01 23:34:45', 0),
(18, 'tester', '123', 'tester@quizmaster.pl', '2017-02-01 23:35:02', 0),
(19, 'kopacz12', '123', 'kopacz@gmail.com', '2017-02-01 23:35:26', 0),
(20, 'krystianQuizowiec', '123', 'krystek@gmail.com', '2017-02-01 23:35:58', 0),
(21, 'monikas', '123', 'monikas@gmail.com', '2017-02-02 01:00:22', 0);

--
-- Indeksy dla zrzutów tabel
--

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`categoryID`);

--
-- Indexes for table `question`
--
ALTER TABLE `question`
  ADD PRIMARY KEY (`questionID`);

--
-- Indexes for table `quiz`
--
ALTER TABLE `quiz`
  ADD PRIMARY KEY (`quizID`);

--
-- Indexes for table `quiz_answer`
--
ALTER TABLE `quiz_answer`
  ADD PRIMARY KEY (`quiz_answerID`);

--
-- Indexes for table `quiz_fact`
--
ALTER TABLE `quiz_fact`
  ADD PRIMARY KEY (`quiz_factID`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`userID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT dla tabeli `category`
--
ALTER TABLE `category`
  MODIFY `categoryID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT dla tabeli `question`
--
ALTER TABLE `question`
  MODIFY `questionID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=653;
--
-- AUTO_INCREMENT dla tabeli `quiz`
--
ALTER TABLE `quiz`
  MODIFY `quizID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=168;
--
-- AUTO_INCREMENT dla tabeli `quiz_answer`
--
ALTER TABLE `quiz_answer`
  MODIFY `quiz_answerID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=218;
--
-- AUTO_INCREMENT dla tabeli `quiz_fact`
--
ALTER TABLE `quiz_fact`
  MODIFY `quiz_factID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=69;
--
-- AUTO_INCREMENT dla tabeli `user`
--
ALTER TABLE `user`
  MODIFY `userID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

var express = require("express");
var app = express();
app.set('view engine', 'ejs');
var server = require('http').Server(app);
var router = express.Router();
var views = __dirname + '/views/';
var path = require("path");
var mysql = require('mysql');
var session = require("express-session");
var bodyParser = require('body-parser');
var async = require('async');

var sess;

var categories_ID = [];
var categories_name = [];

var connection = mysql.createConnection({
	connectionLimit		: 100,
	host				: 'localhost',
	user				: 'root',
	password			: '',
	database			: 'Quizmaster',
	debug    			:  false,
	multipleStatements	: true
});

connection.connect(function(error){
	if(!!error){
		console.log('Error: cannnot connect to MySQL Database!');
	}
	else {
		console.log('Connected to MySQL Database!');
	}
});

router.use(function (req,res,next) {
	sess = req.session;
	res.locals.ID = sess.ID;
	res.locals.login = sess.login;
	res.locals.num_q = sess.num_q;
	res.locals.current = "TESTTT";
	connection.query("SELECT * FROM category", function(error, rows, fields) {
		if(!!error) {
			return console.log('Error in the query [15]');
		}
		else {
			for (i = 0; i < rows.length; i++) {
				categories_ID[i] = rows[i].categoryID;
				categories_name[i] = rows[i].name;
			}
		}
		next();
	});
});

router.use(bodyParser.urlencoded({ extended: true })); 

router.get("/",function(req,res){
	var quiz = [];
	var category_names = [];
	var number_of_questions = [];

	sess = req.session;
	res.locals.current = "/";
	
	connection.query("SELECT * FROM quiz WHERE isAccepted = 1 ORDER BY dateCreated DESC LIMIT 3", function(error, rows, fields) {
		if(!!error) {
			return console.log('Error in the query [100]');
		}
		else {
			for (i = 0; i < rows.length; i++) {
				quiz[i] = rows[i];
				for (n = 0; n < categories_ID.length; n++) {
					if (quiz[i].categoryID == categories_ID[n]){
						category_names[i] = categories_name[n];
					}
				}
			}
			
			var difficulty = [];
			async.forEachOf(quiz, function(myBody, m, eachDone) {
				async.waterfall([
					function(next) {
						connection.query("SELECT * FROM question WHERE quizID = " + quiz[m].quizID, next);
					},
					function(results, fields, next) {
						number_of_questions[m] = results.length;
						var qquery = 	 "SELECT qa.user_answer, que.correct_answer FROM quiz AS q"
											+" INNER JOIN quiz_fact AS qf ON q.quizID = qf.quizID"
											+" INNER JOIN quiz_answer AS qa ON qf.quiz_factID = qa.quiz_factID"
											+" INNER JOIN question AS que ON qa.questionID = que.questionID"
											+" WHERE q.isAccepted = 1 AND q.quizID = " + quiz[m].quizID;
						connection.query(qquery, function(errorr, rowss, fieldss) {
							if(!!errorr) {
								return console.log('Error in the query [111]');
							}
							else {
								var bad_num = 0;
								for (i = 0; i < rowss.length; i++) {
									if (rowss[i].user_answer != rowss[i].correct_answer) {
										bad_num++;
									}
								}
								difficulty[m] = Math.round((bad_num / rowss.length) * 100)
								if (m == quiz.length - 1) {
									var logged = true;
									if (sess.login == undefined || sess.login == "" || sess.login == null) {
										logged = false;
									}
									
									res.render("index.ejs", {quiz:quiz, difficulty:difficulty, category_names:category_names,
										number_of_questions:number_of_questions, logged:logged, categories_name:categories_name});
								}
							}
						});
					}
				], eachDone);
			}, function(err) {
				if (err) {
					console.log("Error: " + err);
				}
			});
		}		
		
	});
});

router.get("/login",function(req,res){
	if (sess.login != null) {
		return res.redirect('/profile');
	}
	sess = req.session;
	res.locals.current = "/login";
	res.render(views + "login.ejs");
});

router.get("/about",function(req,res){
	sess = req.session;
	res.locals.current = "/about";
	res.render(views + "about.ejs");
});

router.get("/quiz_browse",function(req, res){
	sess = req.session;
	var pageID = req.query.p / 1;
	var filter = req.query.f;
	var filter_category = req.query.c;
	var sql_filter;
	var sql_filter_cat;
	var quiz = [];
	var category_names = [];
	var number_of_questions = [];
	
	var first_rec = 0;
	var number_of_records = 0;
	
	var all_pages = 1;
	var pagination = [];
	
	if (Number.isInteger(pageID)){
		if (pageID > 0) {
			first_rec = 9 * (pageID - 1);
		}
		else {
			pageID = 1;
		}
	}
	else {
		pageID = 1;
	}

	sess = req.session;
	
	if (filter_category != "" && filter_category != null && filter_category != undefined){
		if (filter_category > 0 && filter_category <= categories_ID.length ){
			sql_filter_cat = "AND categoryID = " + filter_category + " ";
		}
		else {
			sql_filter_cat = "";
		}
	}
	else {
		sql_filter_cat = "";
	}
	
	switch(filter){
		case "dateasc":
			sql_filter = "ORDER BY dateCreated ASC";
			break;
		case "datedesc":
			sql_filter = "ORDER BY dateCreated DESC";
			break;
		case "nameasc":
			sql_filter = "ORDER BY name ASC";
			break;
		case "namedesc":
			sql_filter = "ORDER BY name DESC";
			break;
		case "catasc":
			sql_filter = "ORDER BY categoryID ASC";
			break;
		case "catdesc":
			sql_filter = "ORDER BY categoryID DESC";
			break;
		default:
			sql_filter = "ORDER BY dateCreated DESC";
	}
	
	connection.query("SELECT * FROM quiz WHERE isAccepted = 1 " + sql_filter_cat + sql_filter, function(error, rows, fields) {
		if(!!error) {
			return console.log('Error in the query [167]');
		}
		else {
			if (rows.length == 0) {
				return res.render("quiz_browse.ejs", {quiz:quiz, pageID:1, category_names:category_names, pag:pagination, filter:"", filter_category:filter_category, error: "Brak quizów w tej kategorii!"});
			}
			var all_quizes = []
			var all_categories = [];
			for (i = 0; i < rows.length; i++) {
				if (rows[i] != undefined && rows[i] != null && rows[i] != ""){
					all_quizes[i] = rows[i];
					for (n = 0; n < categories_ID.length; n++) {
						if (all_quizes[i].categoryID == categories_ID[n]){
							all_categories[i] = categories_name[n];
							all_quizes[i] = rows[i];
						}
					}
				}
			}

			if (filter == "catasc") { // sortuj kategoriami od A do Z
				var n = all_quizes.length;
				var swapped;
				var pp = 0;
				do {
					pp++;
					swapped = false;
					for (var k = 0; k < n - 1; k++) {
						if(!compareStrings(all_categories[k], all_categories[k + 1])) {
							var temp_cat = all_categories[k];
							var temp_quiz = all_quizes[k];
							all_categories[k] = all_categories[k + 1];
							all_quizes[k] = all_quizes[k + 1];
							all_categories[k + 1] = temp_cat;
							all_quizes[k + 1] = temp_quiz;
							swapped = true;
						}
					}
				} while (swapped);
			}
			else if (filter == "catdesc") {  // sortuj kategoriami od Z do A
				var n = all_quizes.length;
				var swapped;
				var pp = 0;
				do {
					pp++;
					swapped = false;
					for (var k = 1; k < n; k++) {
						if(!compareStrings(all_categories[k], all_categories[k - 1])) {
							var temp_cat = all_categories[k];
							var temp_quiz = all_quizes[k];
							all_categories[k] = all_categories[k - 1];
							all_quizes[k] = all_quizes[k - 1];
							all_categories[k - 1] = temp_cat;
							all_quizes[k - 1] = temp_quiz;
							swapped = true;
						}
					}
				} while (swapped);
			}

			number_of_records = all_quizes.length;
			for (i = 0; i < 9; i++) {
				if (all_quizes[i + first_rec] != undefined && all_quizes[i + first_rec] != null && all_quizes[i + first_rec] != ""){
					quiz[i] = all_quizes[i + first_rec];
					category_names[i] = all_categories[i + first_rec];
				}
			}
	
			all_pages = Math.ceil(number_of_records / 9 );
			// Ustalenie paginacji
			if (all_pages < 6) { // jesli nie ma więcej niż 5 stron
				for (i = 0; i < all_pages; i++) {
					pagination[i] = i + 1;
				}
			}
			else { // więcej niż 5 stron
				if (all_pages == pageID || all_pages == pageID + 1){ // jesli obecna strona jest ostatnia lub przedostatnia
					for (i = 0; i < 5; i++) {
						pagination[i] = i + pageID - 4;
					}
				}
				else if (pageID == 1 || pageID == 2){ // jesli obecna strona jest pierwsza lub druga
					for (i = 0; i < all_pages; i++) {
						pagination[i] = i + 1;
					}
				}
				else { // są conajmniej dwie następnie i 2 poprzednie
					pagination[0] = pageID - 2;
					pagination[1] = pageID - 1;
					pagination[2] = pageID;
					pagination[3] = pageID + 1;
					pagination[4] = pageID + 2;
				}
			}
		}
		
		var temp_val = 0;
		res.locals.current = "/quiz_browse";
		if (pageID != 1) {
			temp_val = number_of_records / (9 * (pageID - 1));
			if (temp_val < 1){
				return res.render("quiz_browse.ejs", {quiz:quiz, pageID:1, category_names:category_names, pag:pagination, filter:"", filter_category:"", error:"Wybrana strona nie istnieje!"});
			}
		}
		
		var difficulty = [];
		async.forEachOf(quiz, function(myBody, m, eachDone) {
			async.waterfall([
				function(next) {
					connection.query("SELECT * FROM question WHERE quizID = " + quiz[m].quizID, next);
				},
				function(results, fields, next) {
					number_of_questions[m] = results.length;
					var qquery = 	 "SELECT qa.user_answer, que.correct_answer FROM quiz AS q"
											+" INNER JOIN quiz_fact AS qf ON q.quizID = qf.quizID"
											+" INNER JOIN quiz_answer AS qa ON qf.quiz_factID = qa.quiz_factID"
											+" INNER JOIN question AS que ON qa.questionID = que.questionID"
											+" WHERE q.isAccepted = 1 AND q.quizID = " + quiz[m].quizID;
					connection.query(qquery, function(errorr, rowss, fieldss) {
						if(!!errorr) {
							return console.log('Error in the query [115]');
						}
						else {
							var bad_num = 0;
							for (i = 0; i < rowss.length; i++) {
								if (rowss[i].user_answer != rowss[i].correct_answer) {
									bad_num++;
								}
							}
							difficulty[m] = Math.round((bad_num / rowss.length) * 100)
							
							if (m == quiz.length - 1) {
								var logged = true;
								if (sess.login == undefined || sess.login == "" || sess.login == null) {
									logged = false;
								}
								
								return res.render("quiz_browse.ejs", {quiz:quiz, difficulty:difficulty, pageID:pageID, category_names:category_names, 
									number_of_questions:number_of_questions, pag:pagination, error:"", logged:logged, filter:filter, filter_category:filter_category});
							}
						}
					});
				}
			], eachDone);
		}, function(err) {
			if (err) {
				console.log("Error: " + err);
			}
		});
	});
});

router.get("/ranking",function(req,res){
	sess = req.session;
	res.locals.current = "/ranking";
	var new_query =  "SELECT us.login AS login, COUNT(q.name) AS quiz_num FROM user AS us"
					+" LEFT JOIN quiz AS q ON us.userID = q.userID"
					+" GROUP BY us.login ORDER BY quiz_num DESC LIMIT 100;";
	connection.query(new_query, function(error, rows, fields) {
		if(!!error) {
			return console.log('Error in the query [444]');
		}
		else {
			res.render(views + "ranking.ejs", {users:rows});
		}
	});
});

router.get("/quiz_add",function(req,res){
	var found_categories = [];
	
	if (sess.num_q == null || sess.num_q == "" || sess.num_q == undefined) {
		return res.redirect('/');
	}
	else {
		if (sess.login == null || sess.login == "" || sess.login == undefined) {
			return res.redirect('/');
		}
		else {
			connection.query("SELECT name FROM category ORDER BY name", function(error, rows, fields) {
				if(!!error) {
					console.log('Error in the query [0]');
				}
				else {
					if (rows.length == 0){
						console.log("Error: There is no categories!");
					}
					else {
						for (i = 0; i < rows.length; i++) {
							found_categories[i] = rows[i].name;
						}
						
						sess = req.session;
						res.locals.current = "/quiz_add";
						res.render(views + "quiz_add.ejs", { categories:found_categories });
					}
				}
			});
		}
	}
});

router.get("/quiz_new",function(req,res){
	sess = req.session;
	res.locals.current = "/profile";
	if (sess.login == null || sess.login == "" || sess.login == undefined) {
		return res.redirect('/');
	}
	res.locals.current = "/quiz_new";
	res.render(views + "quiz_new.ejs");
});

router.get("/profile",function(req, res){
	sess = req.session;
	if (sess.login == null || sess.login == "" || sess.login == undefined) {
		return res.redirect('/login');
	}
	
	var new_query =  "SELECT q.name, qf.quiz_factID, qf.quizID, unix_timestamp(qf.date) * 1000 AS date FROM quiz_fact AS qf"
					+" INNER JOIN quiz AS q ON qf.quizID = q.quizID"
					+" WHERE qf.userID = " + sess.ID + ";";
	connection.query(new_query, function(error, result, fields) {
		if(!!error) {
			return console.log('Error in the query [199]');
		}
		else {
			var quiz_facts = result;
			var quiz_facts_questions = new Array(result.length);
			for (var i = 0; i < result.length; i++) {
				quiz_facts_questions[i] = [];
			}
			if (result.length == 0){
				return res.render(views + "profile.ejs", { quiz_facts:quiz_facts, quiz_facts_questions:quiz_facts_questions });
			}
			async.forEachOf(quiz_facts, function(myBody, m, eachDone) {
				async.waterfall([
					function(next) {
						var query_string = 	  "	SELECT q.name, que.body, qa.user_answer, que.correct_answer FROM quiz_fact AS qf"
											 +"	INNER JOIN quiz AS q ON qf.quizID = q.quizID"
											 +"	INNER JOIN quiz_answer AS qa ON qf.quiz_factID = qa.quiz_factID"
											 +"	INNER JOIN question AS que ON qa.questionID = que.questionID"
											 +"	WHERE qf.userID = " + sess.ID + " AND qf.quiz_factID = " + quiz_facts[m].quiz_factID
		
						connection.query(query_string, next);
					},
					function(results, fields, next) {
						quiz_facts_questions[m] = results;
						if (m == quiz_facts.length - 1) {
							res.render(views + "profile.ejs", { quiz_facts:quiz_facts, quiz_facts_questions:quiz_facts_questions });
						}
					}
				], eachDone);
				}, function(err) {
				if (err) {
					console.log("Error query [87]: " + err);
				}
			});
		}
	});
});

router.get("/redirect",function(req, res){
	res.redirect('/');
});

router.get('/logout',function(req, res){
	sess = req.session;
	var temp_login = sess.login;
	req.session.destroy(function(err) {
		if(err) {
			console.log(err);
		} 
		else {
			console.log("User " + temp_login + " has logged off");
			res.redirect('/');
		}
	});
});

router.post('/loginme', function(req, res) {
	var user_login = req.body.login;
	var user_password = req.body.password;
	if (user_login != "" && user_password != "") {
		connection.query("SELECT * FROM user WHERE login='" + user_login + "'", function(error, rows, fields) {
			if(!!error) {
				return console.log('Error in the query [2]');
			}
			else {
				if (rows.length == 0){
					return console.log("Account not exists!")
				}
				else {
					connection.query("SELECT * FROM user WHERE login='" + user_login + "' AND password='" + user_password + "'"
					, function(error, rows, fields) {
						if(!!error) {
							return console.log('Error in the query [3]');
						}
						else {
							if (rows.length == 0){
								return console.log("Invalid password!");
							}
							else { // set session
								sess = req.session;
								sess.login = user_login;
								sess.ID = rows[0].userID;
								sess.num_q = null;
								console.log("User " + sess.login + " logged on!")
								res.redirect('/');
							}
						}
					});
				}
			}
		});		
	}
});

router.post('/regme', function(req, res) {
	sess = req.session;
	var user_login = req.body.login;
	var user_password = req.body.password;
	var user_password2 = req.body.password2;
	var user_email = req.body.email;
	if (user_login != "" && user_password != "" && user_password2 != "" && user_email != "") {
		connection.query("SELECT * FROM user WHERE login='" + user_login + "'", function(error, rows, fields) {
			if(!!error) {
				console.log('Error in the query [0]');
			}
			else {
				if (rows.length == 0){
					if (user_password != user_password2){
						console.log('Incorrect passwords!');
					}
					else {
						var pDate = new Date();
						var pDateSeconds = pDate.valueOf() / 1000;
						connection.query("INSERT INTO quizmaster.user SET userID = NULL" + ", login = '" + user_login + "'"
						+ ", password = '" + user_password + "', email = '" + user_email + "', regDate = FROM_UNIXTIME(" + pDateSeconds + ")"
						+ ", rank = 0", function(error, rows, fields) {
							if(!!error) {
								console.log('Error in the query [1] - ' + error);
							}
							else {
								console.log('Account ' + user_login + " created!");
								res.redirect('/redirect');
							}
						});
					}
				}
				else {
					console.log('Login is taken!');
				}
			}
		});
	}
	else {
		console.log('Fill all fields!');
	}
});

router.post('/addquiz', function(req, res) {
	var quiz_name = req.body.quiz_name;
	var quiz_desc = req.body.quiz_desc;
	var quiz_cat = req.body.quiz_cat;
	var quiz_catID = 0
	var found_quiz_ID = 0;
	
	// add quiz
	if (quiz_name != "" && quiz_desc != "" && quiz_cat != "") {
				var q_questions = [];
				var qq_questions = [];
				
				q_questions[0] = "";
				q_questions[1] = "";
				q_questions[2] = "";
				
				qq_questions[0] = "";
				var iterations = sess.num_q / 1;
				for (i = 1; i <= iterations; i++) {
					qq_questions[i] = req.body.quiz_question[i - 1];
				}
				async.forEachOf(q_questions, function(myBody, m, eachDone) {
					async.waterfall([
						function(next) {
							connection.query("SELECT categoryID FROM category WHERE name = '" + quiz_cat + "'", next);
						},
						function(results, fields, next) {
							if (m == 0) {
								if (results[0].categoryID != undefined) {
									quiz_catID = results[0].categoryID;
									return next();
								}
							}
							else if(m == 2) {
								var temp = [""];
								async.forEachOf(qq_questions, function(myBody2, m2, eachDone2) {
									async.waterfall([
										function(next2) {
											connection.query("SELECT quizID FROM quiz WHERE name = '" + quiz_name + "'", next2);
										},
										function(results2, fields2, next2) {
												if (m2 == 0) {
													if (results2[0].quizID != undefined) {
														found_quiz_ID = results2[0].quizID;
													}
												}
												else {
													var q_body = req.body.quiz_question[m2 - 1];
													var q_ans_A = req.body.quiz_ans_A[m2 - 1];
													var q_ans_B = req.body.quiz_ans_B[m2 - 1];
													var q_ans_C = req.body.quiz_ans_C[m2 - 1];
													var q_ans_D = req.body.quiz_ans_D[m2 - 1];
													var q_answer = req.body.quiz_answer[m2 - 1];

													if (q_body != undefined && q_ans_A != undefined && q_ans_B != undefined && q_ans_C != undefined && q_ans_D != undefined && q_answer != undefined) {
														if (q_body != "" && q_ans_A != "" && q_ans_B != ""&& q_ans_C != "" && q_ans_D != "" && q_answer != "") {
															connection.query("INSERT INTO quizmaster.question SET questionID = NULL" + ", body = '" + q_body
															+ "', A = '" + q_ans_A + "', B = '" + q_ans_B + "', C = '" + q_ans_C + "', D = '" + q_ans_D
															+ "', correct_answer = '" + q_answer + "', categoryID = " + quiz_catID + ", quizID = " + found_quiz_ID, next2);
														}
													}
													
													if (m2 == qq_questions.length - 1){
														console.log("User " + sess.login + " created quiz: " + quiz_name);
														res.redirect('/');
													}
												}
											
										}
									], eachDone2);
									}, function(err2) {
									if (err2) {
										console.log("Error: " + err2);
									}
								});
							}
							else {
								if(m == 1) {
									var pDate = new Date();
									var pDateSeconds = pDate.valueOf() / 1000;
									connection.query("INSERT INTO quizmaster.quiz SET quizID = NULL" + ", name = '" + quiz_name + "'"
									+ ", description = '" + quiz_desc + "', categoryID = " + quiz_catID + ", userID = " + sess.ID
									+ ", isAccepted = FALSE, dateCreated = FROM_UNIXTIME(" + pDateSeconds + ")", next);
								}
							}
						}
					], eachDone);
				}, function(err) {
				if (err) {
					console.log("Error: " + err);
				}
			});
	}
});

router.post('/newquiz', function(req, res) {
	sess = req.session;
	if (sess.login == undefined || sess.login == "" || sess.login == null) {
		return res.redirect('/');
	}
	
	var num_questions = req.body.quiz_answers;
	sess.num_q = num_questions;
	res.redirect('/quiz_add');
});

router.post('/changepw', function(req, res) {
	sess = req.session;
	if (sess.login == undefined || sess.login == "" || sess.login == null) {
		return res.redirect('/');
	}
	
	var pass1 = req.body.password1;
	var pass2 = req.body.password2;
	var pass_new = req.body.password3;
	
	if (pass1 != "" || pass1 != undefined || pass2 != "" || pass2 != undefined || pass_new != "" || pass_new != undefined) {
		if (pass1 == pass2){
			connection.query('UPDATE user SET ? WHERE userID = ?', [{ password:pass_new },sess.ID], function(error, results) {
				
				if(!!error) {
					console.log('Error in the query [98]');
				}
				else {
					console.log("Users: " + sess.login + " password changed!");
					res.redirect('/profile');
				}
			});
		}
		else {
			console.log("Error: password not match");
			return res.redirect('/');
		}
	}
	else {
		console.log("Error: undefinde passwords");
		return res.redirect('/');
	}
});

router.post('/loadquiz', function(req, res) {
	sess = req.session;
	if (sess.login == undefined || sess.login == "" || sess.login == null) {
		return res.redirect('/');
	}
	
	var quiz_ID = req.body.load_quiz_ID;
	var quiz_name = req.body.load_quiz_name;
	
	connection.query("SELECT * FROM question WHERE quizID =" + quiz_ID, function(error, rows, fields) {
		if(!!error) {
			console.log('Error in the query [200]');
		}
		else {
			if (rows.length == 0){
				console.log('Error: No questions found!');
			}
			else {
				var questions = [];
				
				for (i = 0; i < rows.length; i++) {
					questions[i] = rows[i];
				}
				res.render(views + "quiz.ejs", {quiz_ID:quiz_ID, quiz_name:quiz_name, question:questions});
			}
		}
	});
});

router.post('/validate_quiz', function(req, res) {
	sess = req.session;
	if (sess.login == undefined || sess.login == "" || sess.login == null) {
		return res.redirect('/');
	}
	
	var questions_ID = [];
	var questions_answers = [];
	var questions_answers_body = [];
	var questions_body = [];
	
	var sql_query = "";
	
	for (i = 0; i < req.body.radio.length; i++) {
		questions_ID[i] = req.body.get_q_ID[i];
		questions_body[i] = req.body.get_q_body[i];
		questions_answers[i] = req.body.radio[i];
		sql_query = sql_query + "questionID = " + questions_ID[i];
		if (i != req.body.radio.length - 1) { sql_query = sql_query + " OR " };
	}
	
	connection.query("SELECT * FROM question WHERE " + sql_query, function(error, rows, fields) {
		if(!!error) {
			console.log('Error in the query [269]');
		}
		else {
			if (rows.length == 0){
				console.log("Something goes wrong");
			}
			else {
				var correct_answer = [];
				var correct_Body = [];
				var score = 0;
				for (i = 0; i < rows.length; i++) {
					correct_answer[i] = rows[i].correct_answer;
					
					if (questions_answers[i] == "A") { questions_answers_body[i] = rows[i].A;}
					else if (questions_answers[i] == "B") { questions_answers_body[i] = rows[i].B;}
					else if (questions_answers[i] == "C") { questions_answers_body[i] = rows[i].C;}
					else if (questions_answers[i] == "D") { questions_answers_body[i] = rows[i].D;}
					
					if (correct_answer[i] == "A") { correct_Body[i] = rows[i].A;}
					else if (correct_answer[i] == "B") { correct_Body[i] = rows[i].B;}
					else if (correct_answer[i] == "C") { correct_Body[i] = rows[i].C;}
					else if (correct_answer[i] == "D") { correct_Body[i] = rows[i].D;}
					
					if (correct_answer[i] == questions_answers[i]){
						score++;
					}
				}
				
				var pDate = new Date();
				var pDateSeconds = pDate.valueOf() / 1000;
				
				connection.query("INSERT INTO quiz_fact SET quiz_factID = NULL" + ", quizID = '" + req.body.get_quiz_name + "'"
				+ ", userID = '" + sess.ID + "', date = FROM_UNIXTIME(" + pDateSeconds + ")", function(error, results) {
					if(!!error) {
						console.log('Error in the query [1] - ' + error);
					}
					else {
						var quiz_factID = results.insertId;
						async.forEachOf(questions_ID, function(myBody, m, eachDone) {
							async.waterfall([
								function(next) {
									connection.query("INSERT INTO quiz_answer SET quiz_answerID = NULL" + ", quiz_factID = '" + quiz_factID + "'"
									+ ", questionID = " + questions_ID[m] + ", user_answer = '" + questions_answers[m] + "'", next);
								},
								function(results, next) {
									if (m == questions_ID.length - 1) {
										res.render(views + "quiz_result.ejs", {questions_ID:questions_ID, questions_body:questions_body, 
										questions_answers:questions_answers, correct_answer:correct_answer, score:score,
										correct_Body:correct_Body, questions_answers_body:questions_answers_body});
									}
								}
							], eachDone);
						}, function(err) {
							if (err) {
								console.log("Error in inserting answers: " + err);
							}
						});
					}
				});
			}
		}
	});
});

router.post('/redirect', function(req, res) {
	res.redirect('/');
});

app.use(session({
	secret: 'ssshhhhh',
	resave: true,
    saveUninitialized: true
}));

app.use(express.static(__dirname + '/public'));

app.use("/", router);

app.use("*",function(req,res){
	res.sendFile(views + "404.ejs");
});

server.listen(3000, function(){
	console.log("Server started on port 3000! Started date: " + new Date().toISOString()); 
});

// sortowanie

function compareStrings(string1, string2) { // return true if string1 < string2
	var temp_string1;
	var temp_string2;
	
	temp_string1 = string1.toLowerCase();
	temp_string2 = string2.toLowerCase();

    for (var i = 0; i < temp_string1.length; i++) {
		var char1 = temp_string1.charAt(i);
		var char2 = temp_string2.charAt(i);
		if (char1 != char2) {
			var val1 = getValueOfSign(char1);
			var val2 = getValueOfSign(char2);
			if (val1 < val2) {
				return true;
			}
			else {
				return false;
			}
		}
	}
	return true;
}

function getValueOfSign(sign) {
	var signs = "'*!@_.()#^&%-=+01234567989aąbcćdeęfghijklłmnńoóprsśtuwyzźż";
	var temp_value = 0;
	
	for (var i = 0; i < signs.length; i++) {
		if (sign == signs.charAt(i)) {
			temp_value = i;
		}
	}
	return temp_value;
}